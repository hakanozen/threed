angular.module('3dhubsapp', [
    'templates-app',
    'templates-common',
    'ui.router',
    'ngMessages',
    'restangular',
    'ngMaterial',
    'angular-jwt',
    '3dhubsapp.cart'
  ])

  .config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
  }])

  .config(function myAppConfig($stateProvider, $urlRouterProvider, $mdDateLocaleProvider, $httpProvider, jwtInterceptorProvider, RestangularProvider, $mdThemingProvider, $locationProvider ) {

    // Config: Anguar Material Theme
    $mdThemingProvider
    .theme('default')
    .primaryPalette('indigo')
    .accentPalette('pink')
    .warnPalette('red')
    .backgroundPalette('grey');

    // Config: ui-router
    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix("!");
    $urlRouterProvider.otherwise('/cart');

    // Config: jwt
    jwtInterceptorProvider.tokenGetter = function () {
      return 'Kcm5Mm3tM2RodWJzQDNkbml0eS5jb20gMTUwNTMxMzUxNQ==';
    };

    $httpProvider.interceptors.push('jwtInterceptor');

    // Config: Restangular
    RestangularProvider.setBaseUrl('https://3dnity.com/api');

  })

  .run(function run() {

  })

  .controller('AppCtrl', function AppCtrl( $scope ) {


  })

;
