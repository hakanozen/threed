angular.module('3dhubsapp.cart', [
  'ui.router',
  'ngFileUpload',
  '3dhubsapp.services.filesService'
])
.config(function config( $stateProvider, $logProvider ) {
  $logProvider.debugEnabled(false);

  $stateProvider
  .state('cart', {
    url: '/cart',
    abstract: false,
    views: {
      "main": {
        templateUrl: 'cart/views/cart.tpl.html',
        controller: 'CartCtrl'
      }
    }
  });
})

.controller( 'CartCtrl', function CartController( $scope, $rootScope, $mdDialog, $state, Upload, filesService ) {

  // INIT VALS
  $scope.uploadedFiles = [];
  $scope.loadingFiles = true;

  // FILE UPLOAD
  $scope.$watch('files', function () {
    $scope.upload($scope.files);
  });

  $scope.$watch('file', function () {
    if ($scope.file != null) {
      $scope.files = [$scope.file];
    }
  });

  $scope.log = '';

  $scope.upload = function (files) {
    if (files && files.length) {
      $scope.uploading = true;

      var handleUpload = function (resp) {
        $scope.uploading = false;
        $scope.uploadedFiles.push(resp.data);
      };

      var uploadFailed = function (error) {
        $scope.uploading = false;
        console.log(error);
      };

      var updateProgress = function (evt) {
        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total, 10);
        $scope.log = 'progress: ' + progressPercentage + '% ' + evt.config.data.file.name + '\n' + $scope.log;
      };

      for (var i = 0; i < files.length; i++) {
        var file = files[i];
        if (!file.$error) {
          Upload.upload({
            url: 'https://3dnity.com/api/upload/file',
            data: {
              file: file
            }
          }).then(handleUpload, uploadFailed, updateProgress);
        }
      }
    }
  };

  // DELETE CART ITEM
  $scope.deleteItem = function(ev, itemId) {
    var confirm = $mdDialog.confirm()
     .title('Would you like to delete this item?')
     .ariaLabel('Deletion')
     .targetEvent(ev)
     .ok('Yes')
     .cancel('No');

   $mdDialog.show(confirm).then(function() {
     filesService
     .customPUT(
       {
         'quantity': 0
       },
       itemId,
       undefined,
       {}
     )
     .then(function() {
       $scope.uploadedFiles.map(function(item) {
         if (item.id === itemId) {
           item.quantity = 0;
           $scope.$apply();
         }
       });
     });
   }, function() {
     // nothing happens here
   });
  };

  // UPDATE ITEM quantity
  $scope.updateQuantity = function(itemId, itemQuantity) {
    filesService
    .customPUT(
      {
        'quantity': itemQuantity
      },
      itemId,
      undefined,
      {}
    );
  };

  // GET UPLOADED ITEMS
  filesService
  .getList()
  .then(function(response) {
    $scope.loadingFiles = false;
    $scope.uploadedFiles = response;
  }, function(error) {
    console.log(error);
  });

});
