angular.module('3dhubsapp.services.filesService', ['restangular'])
  .service('filesService', ['Restangular', function (Restangular) {
    return Restangular.one('member-files');
  }])
;
